<?php

require_once 'Storage/ImageArray.php';

class ImageList 
{
    protected $_storage;
    
    public function __construct(IImage $imageStorage)
    {
        $this->_storage = $imageStorage;
    }
    
    public function getList()
    {
        return $this->_storage->getList();
    }
}

$image = new ImageList(new ImageArray());
echo json_encode($image->getList());