<?php

require_once 'Storage/ImageArray.php';

class Image 
{
    protected $_storage;
    protected $_imageId;
    
    public function __construct(IImage $imageStorage, $id)
    {
        $this->_storage = $imageStorage;
        $this->_imageId = $id;
    }

    public function getImage()
    {
        return $this->_storage->getItem($this->_imageId);
    }
}

$image = new Image(new ImageArray(), $_GET['id']);
echo json_encode($image->getImage());