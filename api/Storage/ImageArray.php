<?php

require_once 'IImage.php';

class ImageArray implements IImage
{
    private $_storage = array(
        array('id' => 1, 'thumbnail' => '3.jpg', 'title' => 'Title 1'),
        array('id' => 2, 'thumbnail' => '2809.jpg', 'title' => 'Title 2'),
        array('id' => 3, 'thumbnail' => '77744.jpg', 'title' => 'Title 3'),
        array('id' => 4, 'thumbnail' => '772530.jpg', 'title' => 'Title 4')
    );
    
    public function getList()
    {
        return $this->_storage;
    }
    
    public function getItem($id)
    {
        if ($id < 1 || $id > 4) {
            $id = 1;
        }
        return $this->_storage[$id-1];
    }
}