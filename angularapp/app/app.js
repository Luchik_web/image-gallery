'use strict';

var ImageGalleryApp = angular.module(
    'ImageGallery', 
    ['ui.bootstrap'], 
    function($httpProvider){
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
    })
    .config(function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl : 'angularapp/views/gallery/list.html',
                controller : 'GalleryListCtrl'
            })
            .when('/image/:imageId', {
                templateUrl: 'angularapp/views/gallery/item.html',
                controller: 'GalleryItemCtrl',
            })
            .when('/about', {
                templateUrl: 'angularapp/views/static/about.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    });