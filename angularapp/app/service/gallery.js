'use strict';

ImageGalleryApp.factory('GalleryService', function ($http, $routeParams, $log) {
    function _getData(type, imgId) {
        var data,
            url = "api/userdreams";

        switch (type) {
            case 'Gallery':
                url = "api/imageslist.php";
                break;
            case 'Single':
                url = "api/imageitem.php?id=" + imgId;
                break;
        }
        return $http.post(url)
            .then(
                function(resdata) {
                    return resdata.data;
                }, 
                function () {
                  $log.info('Request dismissed at: ' + new Date());
                }
            );
    }

    return {
        getGallery: function () {
            var res = _getData('Gallery');
            return res;
        },
        getSingle: function (imgId) {
            var res = _getData('Single', imgId);
            return res;
        }
    };
});