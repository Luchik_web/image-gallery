
ImageGalleryApp
    .controller('GalleryListCtrl',
        function ($scope, GalleryService, $log) {
            $scope.items = GalleryService.getGallery();
        }
    )
    .controller('GalleryItemModalCtrl',
        function ($scope, $routeParams, $modal, GalleryService, $log) {
            $scope.closeModal = function() {
//                $modal.resolve();
            }
        }
    )
    .controller('GalleryItemCtrl',
        function ($scope, $routeParams, $modal, GalleryService, $log) {
            $scope.img = GalleryService.getSingle($routeParams.imageId);
            $scope.callModal = function() {
                var modal = $modal.open({
                    templateUrl: 'angularapp/views/gallery/modal.html',
                    controller : 'GalleryItemModalCtrl',
                    scope : $scope
                });
            }
        }
    );