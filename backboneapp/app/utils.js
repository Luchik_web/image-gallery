var msg = (function() {

    function _prn(argumentsArray, mode)
    {
        var start = 0, i;
        function isConsoleOn() {
            if (!'console' in window || typeof console === 'undefined') {
                return false;
            }
            return true;
        }

        switch (mode) {
            case 'error':
                if (isConsoleOn()) {
                    console.error('<<<<<<<<<<<<<<<<<<<<<<');
                    for (i = start; i < argumentsArray.length; i++) {
                        console.error(argumentsArray[i]);
                    }
                }
                break;

            case 'warn':
                if (isConsoleOn()) {
                    console.warn('<<<<<<<<<<<<<<<<<<<<<<');
                    for (i = start; i < argumentsArray.length; i++) {
                        console.warn(argumentsArray[i]);
                    }
                }
                break;

            case 'info':
                if (isConsoleOn()) {
                    console.info('<<<<<<<<<<<<<<<<<<<<<<');
                    for (i = start; i < argumentsArray.length; i++) {
                        if ($.browser.msie) {
                            console.info(ToString(argumentsArray[i]));
                        } else {
                            console.info(argumentsArray[i]);
                        }
                    }
                }
                break;

            default:
                if (isConsoleOn()) {
                    console.log('<<<<<<<<<<<<<<<<<<<<<<');
                    if ($.browser.msie) {
                        console.log(ToString(argumentsArray));
                    } else{
                        console.log(argumentsArray);
                    }
                }
                break;
        }
    }

    return {
        prn : function () {
            _prn(arguments);
        }
    };
})();

var utils = (function() {
    var _viewsTmpltUrl = 'backboneapp/views/',
        _viewsTmpltFormat = '.html';
        
    function _formViewTmltPath(viewName)
    {
        return _viewsTmpltUrl + viewName + _viewsTmpltFormat;
    }
    
    return {
        // Asynchronously load templates from separate .html files
        loadTemplate: function(views, callback) {
            var deferreds = [];
            $.each(views, function(index, view) {
                if (window[view]) {
                    deferreds.push($.get(_formViewTmltPath(view), function(data) {
                        window[view].prototype.template = _.template(data);
                    }));
                } else {
                    alert(view + ' not found');
                }
            });
            $.when.apply(null, deferreds).done(callback);
        }        
    };
})();