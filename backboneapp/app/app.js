/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var AppRouter = Backbone.Router.extend({
    routes: {
        '' : 'imageList',
        'about' : 'staticAbout',
        'image/:id' : 'imageSingle',
    },

    initialize: function () {
        this.headerView = new HeaderView();
        $('.header').html(this.headerView.el);
        
        this.footerView = new FooterView();
        $('.footer').html(this.footerView.el);
    },

    imageList: function(page) {
        var imgList = new ImageCollection();
        imgList.fetch({success: function(){
            $('#content').html(new ImageListView({model: imgList}).el);
        }});
    },

    imageSingle: function(id) {
        var img = new Image({'id' : id});
        img.fetch({success: function(){
            $('#content').html(new ImageItemView({model: img}).el);
        }});
    },

    staticAbout: function () {
        if (!this.aboutView) {
            this.aboutView = new AboutView();
        }
        $('#content').html(this.aboutView.el);
    }
});


utils.loadTemplate(
    [
        'HeaderView', 'FooterView', 'AboutView', 'ImageListItemView', 'ImageItemView', 'ImageModalView'
    ], 
    function() {
        app = new AppRouter();
        Backbone.history.start();
    }
);