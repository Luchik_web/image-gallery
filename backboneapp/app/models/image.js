window.Image = Backbone.Model.extend({
    urlRoot: 'api/imageitem.php',
    
    url : function(){
        return this.urlRoot + '?id=' + this.get('id');
    },

    initialize: function () {
        
    },

    defaults: {
        id: null,
        thumbnail: ''
    }
});

window.ImageCollection = Backbone.Collection.extend({
    model: Image,
    url : 'api/imageslist.php'
});