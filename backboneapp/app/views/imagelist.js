/* 
 * Image list
 */
window.ImageListView = Backbone.View.extend({

    initialize: function () {
        this.render();
        //this.model.fetch();
    },

    render: function () {
        var imagesModelsList = this.model.models,
            i, len = imagesModelsList.length;

        $(this.el).html('<ul class="gallery"></ul>');

        for (i in imagesModelsList) {
            $('.gallery', this.el).append(new ImageListItemView({model: imagesModelsList[i]}).render().el);
//            this.model.on('change', this.render);
        }
        return this;
    }
});