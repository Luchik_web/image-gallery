/* 
 * Image view
 */
window.ImageModalView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },

    events: {
        'click .modal' : 'imgClose'
    },

    imgClose: function () {
        var imgModal = new ImageModalView({'model' : this.model});
        $('.modal').hide();
        $('#modal').empty();
    }
});