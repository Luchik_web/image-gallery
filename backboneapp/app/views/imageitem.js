/* 
 * Image view
 */
window.ImageItemView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },

    events: {
        'click img' : 'imgPreview'
    },

    imgPreview: function () {
        var imgModal = new ImageModalView({'model' : this.model});
        $('#modal').show();
        $('#modal').html(imgModal.el);
    }
});